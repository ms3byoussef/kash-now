import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:kash_now/features/login_view.dart';
import 'package:kash_now/features/main/debt_view.dart';
import 'package:kash_now/features/main/main_home_view.dart';
import 'package:kash_now/features/reset/reset2.dart';
import 'package:kash_now/features/sginUp/signup_view1.dart';
import 'package:kash_now/features/sginUp/signup_view3.dart';
import 'package:kash_now/features/sginUp/signup_view4.dart';
import 'package:kash_now/features/sginUp/signup_view5.dart';
import 'package:kash_now/features/sginUp/signup_view6.dart';
import 'package:kash_now/features/splash_view.dart';
import 'package:kash_now/redux/app/app_state.dart';
import 'package:kash_now/redux/store.dart';
import 'package:redux/redux.dart';

import 'features/main/installments_menu.dart';
import 'features/main/money_transfer.dart';
import 'features/main/pay_bills.dart';
import 'features/sginUp/signup_view2.dart';
import 'features/sginUp/signup_view7.dart';

Future<Null> main() async {
  var store = await createStore();

  runApp(MyApp(store));
}

//DevicePreview(builder: (context) =>
class MyApp extends StatefulWidget {
  final Store<AppState> store;

  MyApp(this.store);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: widget.store,
      child: MaterialApp(
        // builder: DevicePreview.appBuilder,
        debugShowCheckedModeBanner: false,
        routes: routes,
        title: 'Kash Now',
        theme: ThemeData(
          canvasColor: Colors.transparent,
          fontFamily: "Cairo",
          textTheme: TextTheme(
            headline1: TextStyle(
              fontFamily: 'Cairo',
              color: Color(0xff4a4a4a),
              fontSize: 22,
              fontWeight: FontWeight.w600,
              fontStyle: FontStyle.normal,
            ),
            headline2: TextStyle(
              fontFamily: 'Cairo',
              color: Color(0xff858585),
              fontSize: 20,
              fontWeight: FontWeight.w600,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
        home: Directionality(
            textDirection: TextDirection.rtl,
            child: Scaffold(body: Center(child: MainHomeView()))),
      ),
    );
  }

  Map<String, WidgetBuilder> routes = {};
}
