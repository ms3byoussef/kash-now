import 'package:kash_now/redux/app/app_reducer.dart';
import 'package:redux/redux.dart';

import 'app/app_state.dart';

createStore() {
  return Store<AppState>(
    appReducer,
    initialState: AppState.initial(),
    middleware: [],
  );
}
