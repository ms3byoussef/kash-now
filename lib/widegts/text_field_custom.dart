import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// ignore: must_be_immutable
class CustomTextField extends StatefulWidget {
  TextInputType? keyboardType;
  String? labelText;
  TextEditingController? controller;
  TextInputAction? textInputAction;
  String? Function(String?)? validation;
  bool? obscure;
  bool? suffix;
  VoidCallback? suffixTap;
  String? suffixIcon;
  bool? readOnly;
  String? hint;
  Widget? preIcon;
  String? errorText;

  CustomTextField(
      {this.keyboardType,
      this.labelText,
      this.controller,
      this.textInputAction,
      this.readOnly,
      this.obscure,
      this.preIcon,
      this.suffix,
      this.suffixTap,
      this.suffixIcon,
      Key? key})
      : super(key: key);

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool secure = true;
  List<TextInputFormatter>? passwordOnlyNum = [];
  @override
  void initState() {
    super.initState();
    if (widget.obscure != null) {
      passwordOnlyNum!.add(FilteringTextInputFormatter.digitsOnly);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // width: double.infinity,

      padding: const EdgeInsets.only(bottom: 20, left: 10),
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: TextFormField(
          style: TextStyle(fontSize: 14, color: Color(0xFF7fa643)),
          controller: widget.controller,
          inputFormatters: passwordOnlyNum,
          textInputAction: widget.textInputAction,
          keyboardType: widget.keyboardType,
          obscureText: widget.obscure == null ? false : secure,
          // readOnly: widget.readOnly ?? false,
          validator: widget.validation,
          cursorColor: Colors.black,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(22),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
              borderSide: BorderSide(color: Color(0xFF7fa643), width: 2.0),
            ),
            floatingLabelBehavior: widget.preIcon == null
                ? FloatingLabelBehavior.auto
                : FloatingLabelBehavior.never,
            focusColor: Color(0xFF7fa643),
            labelText: widget.labelText,
            labelStyle: TextStyle(
              fontFamily: 'Cairo',
              color: Color(0xff707070),
              fontSize: 20,
              fontWeight: FontWeight.w500,
            ),
            prefixIcon: widget.preIcon,
            suffix: widget.suffix == false
                ? widget.obscure == null
                    ? null
                    : Padding(
                        padding: EdgeInsets.only(right: 10, bottom: 3),
                        child: GestureDetector(
                          onTap: () => setState(() {
                            secure = !secure;
                          }),
                          child: secure
                              ? Image.asset(
                                  "assets/icons/noun_Eye_3784172.png",
                                  width: 22.5,
                                  height: 22.5,
                                )
                              : Icon(Icons.visibility_off, color: Colors.black),
                        ),
                      )
                : Padding(
                    padding: EdgeInsets.only(right: 10, bottom: 3),
                    child: GestureDetector(
                      onTap: widget.suffixTap,
                      child: widget.suffix ?? false
                          ? Image.asset(
                              widget.suffixIcon!,
                              fit: BoxFit.contain,
                              width: 30,
                              height: 30,
                            )
                          : Container(),
                    ),
                  ),
            // suffixStyle: AppTheme.whiteText,
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xFF707070), width: 1.0),
              borderRadius: BorderRadius.circular(15),
            ),
          ),
        ),
      ),
    );
  }
}
