import 'package:flutter/material.dart';
import 'package:kash_now/features/sginUp/signup_view3.dart';
import 'package:kash_now/widegts/text_field_custom.dart';

class ResetView2 extends StatefulWidget {
  const ResetView2({Key? key}) : super(key: key);

  @override
  _ResetView2State createState() => _ResetView2State();
}

class _ResetView2State extends State<ResetView2> {
  int selectedIndex = 0;
  TextEditingController password = TextEditingController();
  TextEditingController comfirmPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 10,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.1,
                width: 24,
                // padding: EdgeInsets.all(),
                child: InkWell(
                  child: Image.asset("assets/icons/noun_back_2102859.png"),
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.1,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * .31,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "تأكيد رقم الهاتف",
                      style: TextStyle(
                        fontFamily: 'Cairo',
                        color: Color(0xff4a4a4a),
                        fontSize: 22,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      width: 120,
                      height: 150,
                      child: Image.asset(
                        "assets/icons/v.png",
                        width: 100,
                        height: 120,
                        fit: BoxFit.contain,
                      ),
                      // decoration: BoxDecoration(
                      //     image: DecorationImage(
                      //         image: AssetImage(
                      //           "assets/icons/v.png",
                      //         ),
                      //         fit: BoxFit.contain)),
                    )
                  ],
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.4,
              right: 0,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15),
                alignment: Alignment.topRight,
                height: MediaQuery.of(context).size.height * .5,
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                          "قم بكتابة كود التحقق التي تم إرساله إليك عبر رقم الهاتف الذى ينتهى بـ",
                          style: TextStyle(
                            fontFamily: 'Cairo',
                            color: Color(0xff9b9b9b),
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.normal,
                          )),
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text("+20 1098959774",
                            style: TextStyle(
                              fontFamily: 'Cairo',
                              color: Color(0xff4a4a4a),
                              fontSize: 20,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            )),
                      ),
                      NextBtn(
                        txetColor: Colors.white,
                        text: "تحقق",
                        onTap: () {},
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: Stack(
                          children: <Widget>[
                            Opacity(
                              opacity: 0.09000000357627869,
                              child: new Container(
                                  width: 378,
                                  height: 65,
                                  decoration: new BoxDecoration(
                                      color: Color(0xfff79131),
                                      borderRadius: BorderRadius.circular(15))),
                            ),
                            Positioned.fill(
                              child: Container(
                                alignment: Alignment.center,
                                child: Text("أرسل الكود مرة أخرى",
                                    style: TextStyle(
                                      fontFamily: 'Cairo',
                                      color: Color(0xfff79131),
                                      fontSize: 17,
                                      fontWeight: FontWeight.w600,
                                      fontStyle: FontStyle.normal,
                                    )),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
