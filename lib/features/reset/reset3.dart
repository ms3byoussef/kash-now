import 'package:flutter/material.dart';
import 'package:kash_now/features/sginUp/signup_view3.dart';
import 'package:kash_now/widegts/text_field_custom.dart';

class ResetView3 extends StatefulWidget {
  ResetView3({Key? key}) : super(key: key);

  @override
  _ResetView3State createState() => _ResetView3State();
}

class _ResetView3State extends State<ResetView3> {
  int selectedIndex = 0;
  TextEditingController password = TextEditingController();
  TextEditingController comfirmPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 10,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.1,
                width: 24,
                // padding: EdgeInsets.all(),
                child: InkWell(
                  child: Image.asset("assets/icons/noun_back_2102859.png"),
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.15,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * .13,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "  إعادة تعيين الرقم السرى",
                      style: TextStyle(
                        fontFamily: 'Cairo',
                        color: Color(0xff4a4a4a),
                        fontSize: 22,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "قم بتعيين الرقم السرى الجديد الخص بك",
                      style: TextStyle(
                        fontFamily: 'Cairo',
                        color: Color(0xff858585),
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.30,
              right: 0,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15),
                alignment: Alignment.topRight,
                height: MediaQuery.of(context).size.height * .6,
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Stack(
                        children: <Widget>[
                          Opacity(
                            opacity: 0.09000000357627869,
                            child: Container(
                                width: 55,
                                height: 70,
                                decoration: new BoxDecoration(
                                    color: Color(0xffe70744),
                                    borderRadius: BorderRadius.circular(7))),
                          ),
                          Positioned.fill(
                            child: Container(
                              width: 30,
                              height: 40,
                              child: Image.asset(
                                "assets/icons/f.png",
                                width: 10,
                                height: 10,
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                          "الرقم السرى يجب أن يكون 5 أرقام ليست متسلسلة وغير متكررة\n  وستتمكن من استخدامها بالتطبيق لتأكيد أي طلب",
                          style: TextStyle(
                            fontFamily: 'Cairo',
                            color: Color(0xffe70744),
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.normal,
                          )),
                      SizedBox(
                        height: 30,
                      ),
                      CustomTextField(
                        obscure: true,
                        suffix: false,
                        controller: password,
                        labelText: "الرقم السرى",
                        keyboardType: TextInputType.visiblePassword,
                        textInputAction: TextInputAction.next,
                      ),
                      CustomTextField(
                        obscure: true,
                        suffix: false,
                        controller: comfirmPassword,
                        labelText: " تأكيد الرقم السرى",
                        keyboardType: TextInputType.visiblePassword,
                        textInputAction: TextInputAction.done,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      NextBtn(
                        txetColor: Colors.white,
                        text: "تأكيد ",
                        onTap: () {},
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
