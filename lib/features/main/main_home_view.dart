import 'package:flutter/material.dart';
import 'package:kash_now/features/login_view.dart';

class MainHomeView extends StatefulWidget {
  const MainHomeView({Key? key}) : super(key: key);

  @override
  _MainHomeViewState createState() => _MainHomeViewState();
}

class _MainHomeViewState extends State<MainHomeView> {
  bool? isEmpty = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 34,
              child: Container(
                  height: 78,
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.symmetric(horizontal: 25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("مرحبا بك",
                              style: TextStyle(
                                fontFamily: 'Cairo',
                                color: Color(0xffafafaf),
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              )),
                          Text("خالد محسن",
                              style: TextStyle(
                                fontFamily: 'Cairo',
                                color: Color(0xff4a4a4a),
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              ))
                        ],
                      ),
                      // SizedBox(
                      //   width: 90,
                      // ),
                      Padding(
                        padding: const EdgeInsets.only(right: 90),
                        child: LangWidget(),
                      )
                    ],
                  )),
            ),
            Positioned(
              top: 30 + 70 + 30,
              right: -8,
              child: Container(
                  height: 125,
                  width: MediaQuery.of(context).size.width,
                  // margin: EdgeInsets.symmetric(horizontal: 25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: 110.744140625,
                        height: 124.9239730834961,
                        decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(17)),
                        child: Image.asset(
                          "assets/home-icons/home_money.png",
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(width: 36),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("المبلغ المتاح",
                              style: TextStyle(
                                fontFamily: 'Cairo',
                                color: Color(0xff4a4a4a),
                                fontSize: 17,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              )),
                          SizedBox(height: 9),
                          Text("1,500",
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                color: Color(0xff5eb300),
                                fontSize: 35,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )),
                        ],
                      ),
                      // SizedBox(
                      //   width: 90,
                      // ),
                    ],
                  )),
            ),
            Positioned(
              top: 40 + 70 + 30 + 30 + 120,
              child: Container(
                  height: 130,
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.symmetric(horizontal: 22),
                  child: Row(
                    children: [
                      IconWidgetInHome(
                        title: "سلفة من الراتب",
                        color: Color(0xfff79131),
                        icon: "assets/home-icons/home_person.png",
                      ),
                      SizedBox(width: 45),
                      IconWidgetInHome(
                        title: "إدفع فواتيرك",
                        color: Color(0xff9431f7),
                        icon: "assets/home-icons/home_fee.png",
                      ),
                      SizedBox(width: 30),
                      IconWidgetInHome(
                        title: "تحويل أموال",
                        color: Color(0xffe9f5fd),
                        icon: "assets/home-icons/home_transfer.png",
                      ),
                    ],
                  )),
            ),
            Positioned(
              top: 40 + 70 + 30 + 30 + 120 + 120 + 30,
              child: Container(
                  height: 400,
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                              width: 6,
                              height: 17,
                              decoration: new BoxDecoration(
                                  color: Color(0xffebebeb),
                                  borderRadius: BorderRadius.circular(3))),
                          SizedBox(width: 10),
                          Text("طلباتك",
                              style: TextStyle(
                                fontFamily: 'Cairo',
                                color: Color(0xff4a4a4a),
                                fontSize: 17,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )),
                        ],
                      ),
                      Container(
                          height: 366,
                          child: isEmpty == true
                              ? ListView(
                                  children: [
                                    OrderCard(
                                      status: "pending",
                                    ),
                                    OrderCard(
                                      status: "doing",
                                    ),
                                    SizedBox(
                                      height: 200,
                                    )
                                  ],
                                )
                              : Padding(
                                  padding: const EdgeInsets.only(left: 100),
                                  child: SingleChildScrollView(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        SizedBox(
                                          height: 30,
                                        ),
                                        Container(
                                          alignment: Alignment.center,
                                          width: 70,
                                          height: 62,
                                          margin: EdgeInsets.only(
                                            bottom: 12,
                                          ),
                                          child: Image.asset(
                                              "assets/home-icons/empty_order.png"),
                                        ),
                                        Text("لا يوجد لديك أى طلبات",
                                            style: TextStyle(
                                              fontFamily: 'Cairo',
                                              color: Color(0xff8d8d8d),
                                              fontSize: 15,
                                              fontWeight: FontWeight.w600,
                                              fontStyle: FontStyle.normal,
                                            )),
                                      ],
                                    ),
                                  ),
                                ))
                    ],
                  )),
            ),
            Positioned(
              bottom: 24,
              child: Container(
                width: 368,
                height: 82,
                margin: EdgeInsets.symmetric(horizontal: 14),
                decoration: new BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  gradient: LinearGradient(
                    colors: [Color(0xfff79131), Color(0xfff9a01b)],
                    stops: [0, 1],
                    begin: Alignment(-0.97, -0.22),
                    end: Alignment(0.97, 0.22),
                    // angle: 103,
                    // scale: undefined,
                  ),
                ),
                child: Row(
                  children: [
                    SizedBox(width: 20),
                    Stack(
                      children: <Widget>[
                        Opacity(
                          opacity: 0.12999999523162842,
                          child: new Container(
                              width: 134,
                              height: 51,
                              decoration: new BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(16))),
                        ),
                        Positioned.fill(
                          child: Container(
                            child: Row(
                              children: [
                                Container(
                                  width: 23.591796875,
                                  height: 23.5919189453125,
                                  margin: EdgeInsets.symmetric(horizontal: 16),
                                  child: Image.asset(
                                      "assets/home-icons/filled_home.png"),
                                ),
                                new Text("الرئيسية",
                                    style: TextStyle(
                                      fontFamily: 'Cairo',
                                      color: Colors.white,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w600,
                                      fontStyle: FontStyle.normal,
                                    ))
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(width: 42),
                    new Container(
                      width: 30.93994140625,
                      height: 33.01190185546875,
                      child: Image.asset("assets/home-icons/installments.png"),
                    ),
                    SizedBox(width: 55),
                    new Container(
                      width: 34.37841796875,
                      height: 10.2607421875,
                      child: Image.asset("assets/home-icons/more.png"),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class OrderCard extends StatelessWidget {
  String? status;
  OrderCard({this.status, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 195,
      margin: EdgeInsets.only(
        top: 8,
        bottom: 8,
      ),
      decoration: new BoxDecoration(
          color: Color(0xfff9f9f9), borderRadius: BorderRadius.circular(18)),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text("المبلغ المطلوب",
                            style: TextStyle(
                              fontFamily: 'Cairo',
                              color: Color(0xffaaaaaa),
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            )),
                        SizedBox(
                          width: 16,
                        ),
                        Text("500",
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              color: Color(0xff4a4a4a),
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            )),
                        SizedBox(
                          width: 7,
                        ),
                        Text("جنيها",
                            style: TextStyle(
                              fontFamily: 'Cairo',
                              color: Color(0xff4a4a4a),
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            ))
                      ],
                    ),
                    SizedBox(
                      height: 11,
                    ),
                    Row(
                      children: [
                        new Text("مبلغ الإستلام",
                            style: TextStyle(
                              fontFamily: 'Cairo',
                              color: Color(0xffaaaaaa),
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            )),
                        SizedBox(
                          width: 27,
                        ),
                        Text("470",
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              color: Color(0xff4a4a4a),
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            )),
                        SizedBox(
                          width: 7,
                        ),
                        Text("جنيها",
                            style: TextStyle(
                              fontFamily: 'Cairo',
                              color: Color(0xff4a4a4a),
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            ))
                      ],
                    ),
                    SizedBox(
                      height: 11,
                    ),
                    Row(
                      children: [
                        new Text("موعد الطلب",
                            style: TextStyle(
                              fontFamily: 'Cairo',
                              color: Color(0xffaaaaaa),
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            )),
                        SizedBox(
                          width: 35,
                        ),
                        Text("12-10-2022",
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              color: Color(0xff4a4a4a),
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                            )),
                      ],
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          new Container(
                              width: 1,
                              height: 15,
                              decoration: new BoxDecoration(
                                color: Color(0xffaaaaaa),
                              )),
                          SizedBox(
                            width: 25,
                          ),
                          Text("الأقساط",
                              style: TextStyle(
                                fontFamily: 'Cairo',
                                color: Color(0xffaaaaaa),
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              )),
                          SizedBox(
                            width: 12,
                          ),
                          Text("3",
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                color: Color(0xff4a4a4a),
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              )),
                        ],
                      ),
                      SizedBox(
                        height: 11,
                      ),
                      Row(
                        children: [
                          new Container(
                              width: 1,
                              height: 15,
                              decoration: new BoxDecoration(
                                color: Color(0xffaaaaaa),
                              )),
                          SizedBox(
                            width: 25,
                          ),
                          Text("القسط",
                              style: TextStyle(
                                fontFamily: 'Cairo',
                                color: Color(0xffaaaaaa),
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              )),
                          SizedBox(
                            width: 12,
                          ),
                          Text("150",
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                color: Color(0xff4a4a4a),
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              ))
                        ],
                      ),
                      SizedBox(
                        height: 11,
                      ),
                      Row(
                        children: [
                          new Container(
                              width: 1,
                              height: 15,
                              decoration: new BoxDecoration(
                                color: Color(0xffaaaaaa),
                              )),
                          SizedBox(
                            width: 25,
                          ),
                          Text("الرسوم",
                              style: TextStyle(
                                fontFamily: 'Cairo',
                                color: Color(0xffaaaaaa),
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              )),
                          SizedBox(
                            width: 12,
                          ),
                          Text("25",
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                color: Color(0xff4a4a4a),
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              ))
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          status == "pending" ? CloseCardHomeWidget() : DoingCardHomeWidget(),
        ],
      ),
    );
  }
}

class CloseCardHomeWidget extends StatelessWidget {
  const CloseCardHomeWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 330,
      height: 52,
      margin: EdgeInsets.only(left: 10),
      decoration: new BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(10)),
      child: Row(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: Text("الطلب قيد المراجعة",
                style: TextStyle(
                  fontFamily: 'Cairo',
                  color: Color(0xffe70744),
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal,
                )),
          ),
          SizedBox(
            width: 18,
          ),
          InkWell(
            onTap: () {
              _showCancelOrderBottomSheet(context);
            },
            child: Container(
              width: 134,
              height: 39,
              decoration: new BoxDecoration(
                  color: Color(0xffe70744),
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(width: 21),
                  Stack(
                    children: <Widget>[
                      Container(
                        width: 22,
                        height: 22,
                        // child: Image.asset("assets/icons/Ellipse 965.png"),
                        decoration: BoxDecoration(
                            color: Color(0xffEF5982),
                            borderRadius: BorderRadius.circular(30)),
                      ),
                      Positioned.fill(
                        child: Container(
                          width: 22,
                          height: 22,
                          child: Icon(
                            Icons.close_sharp,
                            color: Colors.white,
                            size: 15,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(width: 4),
                  new Text("إلغاء الطلب",
                      style: TextStyle(
                        fontFamily: 'Cairo',
                        color: Colors.white,
                        fontSize: 13,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ))
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _showCancelOrderBottomSheet(context) {
    return showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Directionality(
              textDirection: TextDirection.rtl,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                      width: 73,
                      height: 3,
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                              color: Color(0x247d7d7d),
                              offset: Offset(0, 0),
                              blurRadius: 11,
                              spreadRadius: 0)
                        ],
                      )),
                  SizedBox(height: 6),
                  Container(
                    width: 428,
                    height: 265,
                    // margin: EdgeInsets.symmetric(horizontal: 25),
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)),
                      boxShadow: [
                        BoxShadow(
                            color: Color(0x247d7d7d),
                            offset: Offset(0, 0),
                            blurRadius: 11,
                            spreadRadius: 0)
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 25),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 49),
                          Row(
                            children: [
                              Container(
                                width: 30,
                                height: 30,
                                margin: EdgeInsets.only(left: 9),
                                child:
                                    Image.asset("assets/home-icons/cancel.png"),
                              ),
                              Text("تأكيد إلغاء الطلب",
                                  style: TextStyle(
                                    fontFamily: 'Cairo',
                                    color: Color(0xff4a4a4a),
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600,
                                    fontStyle: FontStyle.normal,
                                  )),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 17, bottom: 38),
                            child: Text("هل أنت متأكد من إلغاء هذا الطلب ؟",
                                style: TextStyle(
                                  fontFamily: 'Cairo',
                                  color: Color(0xff9b9b9b),
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                  fontStyle: FontStyle.normal,
                                )),
                          ),
                          GestureDetector(
                            onTap: () {},
                            child: Container(
                              width: 378,
                              height: 65,
                              decoration: new BoxDecoration(
                                color: Color(0xffe70744),
                                borderRadius: BorderRadius.circular(15),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0x247d7d7d),
                                      offset: Offset(0, 0),
                                      blurRadius: 1,
                                      spreadRadius: 0)
                                ],
                              ),
                              child: Text("إلغاء الطلب",
                                  style: TextStyle(
                                    fontFamily: 'Cairo',
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal,
                                  )),
                              alignment: Alignment.center,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ));
        });
  }
}

class DoingCardHomeWidget extends StatelessWidget {
  const DoingCardHomeWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 330,
      height: 52,
      margin: EdgeInsets.only(left: 15),
      decoration: new BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(10)),
      child: Row(
        children: [
          SizedBox(
            width: 18,
          ),
          new Container(
            width: 25.82373046875,
            height: 22.1385498046875,
            child: Image.asset("assets/home-icons/inprogress.png"),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: new Text("الطلب قيد التنفيذ",
                style: TextStyle(
                  fontFamily: 'Cairo',
                  color: Color(0xff5eb300),
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal,
                )),
          ),
        ],
      ),
    );
  }
}

class TransferedCardHomeWidget extends StatelessWidget {
  TransferedCardHomeWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 330,
      height: 52,
      margin: EdgeInsets.only(left: 15),
      decoration: new BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(10)),
      child: Row(
        children: [
          SizedBox(
            width: 18,
          ),
          new Container(
            width: 25.82373046875,
            height: 22.1385498046875,
            child: Image.asset("assets/home-icons/done.png"),
          ),
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: new Text("تم تحويل المبلغ بتاريخ",
                  style: TextStyle(
                    fontFamily: 'Cairo',
                    color: Color(0xfff79131),
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.normal,
                  ))),
          Text("12-10-2022",
              style: TextStyle(
                fontFamily: 'Cairo',
                color: Color(0xfff79131),
                fontSize: 15,
                fontWeight: FontWeight.w600,
                fontStyle: FontStyle.normal,
              ))
        ],
      ),
    );
  }
}

class IconWidgetInHome extends StatelessWidget {
  Color? color;
  String? icon;
  String? title;
  IconWidgetInHome({this.title, this.color, this.icon, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 122,
      width: 94,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Stack(
            children: <Widget>[
              Opacity(
                opacity: 0.09000000357627869,
                child: new Container(
                    width: 78,
                    height: 80,
                    decoration: new BoxDecoration(
                        color: color, borderRadius: BorderRadius.circular(15))),
              ),
              Positioned.fill(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 21, vertical: 19),
                  child: Image.asset(icon!),
                ),
              ),
            ],
          ),
          SizedBox(height: 16),
          Text(title!,
              style: TextStyle(
                fontFamily: 'Cairo',
                color: Color(0xff616161),
                fontSize: 14,
                fontWeight: FontWeight.w600,
                fontStyle: FontStyle.normal,
              ))
        ],
      ),
    );
  }
}
