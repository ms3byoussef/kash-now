import 'package:flutter/material.dart';
import 'package:kash_now/features/sginUp/signup_view3.dart';
import 'package:kash_now/widegts/text_field_custom.dart';

class DebtView extends StatefulWidget {
  const DebtView({Key? key}) : super(key: key);

  @override
  _DebtViewState createState() => _DebtViewState();
}

class _DebtViewState extends State<DebtView> {
  bool isSelect = true;
  int money = 0;
  TextEditingController password = TextEditingController();
  TextEditingController comfirmPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 10,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * .04,
                width: 24,
                // padding: EdgeInsets.all(),
                child: InkWell(
                  child: Image.asset("assets/icons/noun_back_2102859.png"),
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.05 + 36,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * .34,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    new Text("التقدم بطلب سلفة من الراتب",
                        style: TextStyle(
                          fontFamily: 'Cairo',
                          color: Color(0xff4a4a4a),
                          fontSize: 22,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        )),
                    SizedBox(
                      height: 7,
                    ),
                    new Text("قم بملأ البيانات التالية لتقديم طلبك",
                        style: TextStyle(
                          fontFamily: 'Cairo',
                          color: Color(0xff9b9b9b),
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          fontStyle: FontStyle.normal,
                        )),
                  ],
                ),
              ),
            ),
            Positioned(
              top: 30 + 70 + 30 + 45,
              right: -8,
              child: Container(
                  height: 125,
                  width: MediaQuery.of(context).size.width,
                  // margin: EdgeInsets.symmetric(horizontal: 25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: 110.744140625,
                        height: 124.9239730834961,
                        decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(17)),
                        child: Image.asset(
                          "assets/home-icons/home_money.png",
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(width: 36),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("المبلغ المتاح",
                              style: TextStyle(
                                fontFamily: 'Cairo',
                                color: Color(0xff4a4a4a),
                                fontSize: 17,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              )),
                          SizedBox(height: 9),
                          Text("1,500",
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                color: Color(0xff5eb300),
                                fontSize: 35,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )),
                        ],
                      ),
                      // SizedBox(
                      //   width: 90,
                      // ),
                    ],
                  )),
            ),
            Positioned(
              top: 30 + 70 + 30 + 45 + 125 + 25,
              child: Container(
                  height: MediaQuery.of(context).size.height * 3,
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.symmetric(horizontal: 25),
                  child: SingleChildScrollView(
                    physics: NeverScrollableScrollPhysics(),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("أولا : المبلغ المطلوب",
                            style: TextStyle(
                              fontFamily: 'Cairo',
                              color: Color(0xff4a4a4a),
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            )),
                        SizedBox(height: 9),
                        InkWell(
                          onTap: () {
                            _showConfirmBottomSheet(context);
                          },
                          child: Text("قم بتحديد المبلغ المطلوب",
                              style: TextStyle(
                                fontFamily: 'Cairo',
                                color: Color(0xff9b9b9b),
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                        SizedBox(height: 16),
                        Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 9),
                              child: Text(money.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Cairo',
                                    color: money == 0
                                        ? Color(0xff868984)
                                        : Color(0xff5eb300),
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600,
                                    fontStyle: FontStyle.normal,
                                  )),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 40),
                              child: Text("جنيها",
                                  style: TextStyle(
                                    fontFamily: 'Cairo',
                                    color: money == 0
                                        ? Color(0xff868984)
                                        : Color(0xff5eb300),
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600,
                                    fontStyle: FontStyle.normal,
                                  )),
                            ),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  money = money + 100;
                                });
                              },
                              child: Stack(
                                children: <Widget>[
                                  Opacity(
                                    opacity: 0.12999999523162842,
                                    child: new Container(
                                        width: 35,
                                        height: 35,
                                        decoration: new BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            color: Color(0xfff79131))),
                                  ),
                                  Positioned.fill(
                                    child: Container(
                                      child: Icon(Icons.add,
                                          size: 20, color: Color(0xfff79131)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  money = money - 100;
                                });
                              },
                              child: Container(
                                margin: EdgeInsets.symmetric(horizontal: 12),
                                width: 35,
                                height: 35,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 1,
                                      color: Color(0xfff79131),
                                    ),
                                    borderRadius: BorderRadius.circular(50)),
                                child: Icon(Icons.remove,
                                    size: 18, color: Color(0xfff79131)),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 9),
                          child: new Text("ثانيا : نظام القسط",
                              style: TextStyle(
                                fontFamily: 'Cairo',
                                color: Color(0xff4a4a4a),
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                        Text("قم باختيار القسط المناسب لك",
                            style: TextStyle(
                              fontFamily: 'Cairo',
                              color: Color(0xff9b9b9b),
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            )),
                        SizedBox(height: 20),
                        isSelect
                            ? Container(
                                height: 87,
                                child: ListView(
                                  children: [
                                    MonthlyFeesCard(
                                      isSelect: false,
                                      text: "أشهر",
                                      no: "3",
                                    ),
                                    MonthlyFeesCard(
                                      isSelect: false,
                                      text: "أشهر",
                                      no: "6",
                                    ),
                                    MonthlyFeesCard(
                                      isSelect: false,
                                      text: "أشهر",
                                      no: "9",
                                    ),
                                    MonthlyFeesCard(
                                      isSelect: false,
                                      text: "سنة",
                                      no: "1",
                                    )
                                  ],
                                  scrollDirection: Axis.horizontal,
                                ),
                              )
                            : Container(
                                height: 140,
                                child: ListView(
                                  children: [
                                    MonthlyFeesCard(
                                      isSelect: false,
                                      text: "أشهر",
                                      no: "3",
                                    ),
                                    MonthlyFeesCard(
                                      isSelect: false,
                                      text: "أشهر",
                                      no: "3",
                                    ),
                                    MonthlyFeesCard(
                                      isSelect: false,
                                      text: "أشهر",
                                      no: "3",
                                    )
                                  ],
                                  scrollDirection: Axis.horizontal,
                                ),
                              ),
                        SizedBox(
                          height: 20,
                        ),
                        new Container(
                            width: 340,
                            height: 1,
                            decoration:
                                new BoxDecoration(color: Color(0xfff0f0f0))),
                        Padding(
                          padding: const EdgeInsets.only(top: 18.0, bottom: 12),
                          child: new Text("تفاصيل الطلب",
                              style: TextStyle(
                                fontFamily: 'Cairo',
                                color: Color(0xfff9a01b),
                                fontSize: 17,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                        Container(
                            width: 345,
                            height: 201,
                            decoration: new BoxDecoration(
                                color: Color(0xfff9f9f9),
                                borderRadius: BorderRadius.circular(18))),
                        Container(
                            width: double.infinity,
                            height: 65,
                            margin: EdgeInsets.only(left: 50),
                            decoration: new BoxDecoration(
                                color: Color(0xffececec),
                                borderRadius: BorderRadius.circular(15))),
                        SizedBox(
                          height: 200,
                        ),
                      ],
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }

  _showConfirmBottomSheet(context) {
    return showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Directionality(
              textDirection: TextDirection.rtl,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                      width: 73,
                      height: 3,
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                              color: Color(0x247d7d7d),
                              offset: Offset(0, 0),
                              blurRadius: 11,
                              spreadRadius: 0)
                        ],
                      )),
                  SizedBox(height: 6),
                  Container(
                    width: 428,
                    height: 330,
                    // margin: EdgeInsets.symmetric(horizontal: 25),
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)),
                      boxShadow: [
                        BoxShadow(
                            color: Color(0x247d7d7d),
                            offset: Offset(0, 0),
                            blurRadius: 11,
                            spreadRadius: 0)
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 25),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 39),
                          Row(
                            children: [
                              Text("تأكيد إرسال الطلب",
                                  style: TextStyle(
                                    fontFamily: 'Cairo',
                                    color: Color(0xff4a4a4a),
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600,
                                    fontStyle: FontStyle.normal,
                                  )),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 17, bottom: 24),
                            child: Text(
                                "قم بكتابة الرقم السرى الخاص بحسابك للتأكيد",
                                style: TextStyle(
                                  fontFamily: 'Cairo',
                                  color: Color(0xff9b9b9b),
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                  fontStyle: FontStyle.normal,
                                )),
                          ),
                          CustomTextField(
                            obscure: true,
                            suffix: false,
                            controller: comfirmPassword,
                            labelText: " أدخل الرقم السرى",
                            keyboardType: TextInputType.visiblePassword,
                            textInputAction: TextInputAction.done,
                          ),
                          GestureDetector(
                            onTap: () {},
                            child: new Container(
                              width: 378,
                              height: 65,
                              decoration: new BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                gradient: LinearGradient(
                                  colors: [
                                    Color(0xfff79131),
                                    Color(0xfff9a01b)
                                  ],
                                  stops: [0, 1],
                                  begin: Alignment(-0.99, -0.10),
                                  end: Alignment(0.99, 0.10),
                                  // angle: 96,
                                  // scale: undefined,
                                ),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0x247d7d7d),
                                      offset: Offset(0, 0),
                                      blurRadius: 11,
                                      spreadRadius: 0)
                                ],
                              ),
                              child: Text("تأكيد ",
                                  style: TextStyle(
                                    fontFamily: 'Cairo',
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal,
                                  )),
                              alignment: Alignment.center,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ));
        });
  }
}

class MonthlyFeesCard extends StatefulWidget {
  String? no;
  String? text;
  bool? isSelect;

  MonthlyFeesCard({this.no, this.text, this.isSelect, Key? key})
      : super(key: key);

  @override
  _MonthlyFeesCardState createState() => _MonthlyFeesCardState();
}

class _MonthlyFeesCardState extends State<MonthlyFeesCard> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          widget.isSelect = true;
          widget.isSelect = widget.isSelect;
        });
      },
      child: Container(
        width: 80,
        height: 80,
        margin: EdgeInsets.only(left: 10),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 1),
        decoration: new BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(36),
              topRight: Radius.circular(36),
              bottomLeft: Radius.circular(36),
              bottomRight: Radius.circular(36)),
          color: Colors.white,
          border: Border.all(width: 1, color: Color(0x59d9d9d9)),
          boxShadow: [
            BoxShadow(
                color: Color(0x59d9d9d9),
                offset: Offset(0, 0),
                blurRadius: 20,
                spreadRadius: 0)
          ],
        ),
        child: Column(
          children: [
            Text(widget.no!,
                style: TextStyle(
                  fontFamily: 'Cairo',
                  color: Colors.black,
                  fontSize: 19,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal,
                )),
            Text(widget.text!,
                style: TextStyle(
                  fontFamily: 'Cairo',
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal,
                )),
          ],
        ),
      ),
    );
  }
}

class m extends StatelessWidget {
  const m({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: MediaQuery.of(context).size.height * 0.48,
      right: 0,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15),
        alignment: Alignment.topRight,
        height: MediaQuery.of(context).size.height * .48,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("قم بكتابة كود التحقق التي تم إرساله إليك عبر\n رقم الهاتف",
                  style: TextStyle(
                    fontFamily: 'Cairo',
                    color: Color(0xff9b9b9b),
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.normal,
                  )),
              SizedBox(
                height: 30,
              ),
              Row(
                children: [
                  GestureDetector(
                    onTap: () {},
                    child: Stack(
                      children: <Widget>[
                        Opacity(
                          opacity: 0.09000000357627869,
                          child: new Container(
                              width: 37,
                              height: 37,
                              decoration: new BoxDecoration(
                                  color: Color(0xfff79131),
                                  borderRadius: BorderRadius.circular(10))),
                        ),
                        Positioned.fill(
                          child: Container(
                            padding: EdgeInsets.all(8.9),
                            width: 19.1632080078125,
                            height: 19.16265869140625,
                            child: Image.asset(
                              "assets/icons/noun_edit_1372808.png",
                              width: 18,
                              height: 18,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text("+20 1098959774",
                        style: TextStyle(
                          fontFamily: 'Cairo',
                          color: Color(0xff4a4a4a),
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                          fontStyle: FontStyle.normal,
                        )),
                  ),
                ],
              ),
              SizedBox(
                height: 17,
              ),
              SizedBox(height: 20),
              NextBtn(
                txetColor: Colors.white,
                text: "تحقق",
                onTap: () {},
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 20),
                child: Text("إنتظر لطلب إرسال كود مرة أخرى إذا لم يصلك",
                    style: TextStyle(
                      fontFamily: 'Cairo',
                      color: Color(0xff4a4a4a),
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                    )),
              ),
              Stack(
                children: <Widget>[
                  Opacity(
                    opacity: 0.09000000357627869,
                    child: new Container(
                        width: 378,
                        height: 65,
                        decoration: new BoxDecoration(
                            color: Color(0xfff79131),
                            borderRadius: BorderRadius.circular(15))),
                  ),
                  Positioned.fill(
                    child: MaterialButton(
                      onPressed: () {},
                      child: new Text(
                        "أرسل الكود مرة أخرى",
                        style: TextStyle(
                          fontFamily: 'Cairo',
                          color: Color(0xfff79131),
                          fontSize: 17,
                          fontWeight: FontWeight.w600,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 30),
            ],
          ),
        ),
      ),
    );
  }
}
