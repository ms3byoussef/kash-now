import 'package:flutter/material.dart';
import 'package:kash_now/features/sginUp/signup_view3.dart';
import 'package:kash_now/widegts/text_field_custom.dart';

class MoneyTransfer extends StatefulWidget {
  MoneyTransfer({Key? key}) : super(key: key);

  @override
  _MoneyTransferState createState() => _MoneyTransferState();
}

class _MoneyTransferState extends State<MoneyTransfer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 10,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.1,
                width: 24,
                // padding: EdgeInsets.all(),
                child: InkWell(
                  child: Image.asset("assets/icons/noun_back_2102859.png"),
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.15,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * .1,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    new Text("تحويل أموال",
                        style: TextStyle(
                          fontFamily: 'Cairo',
                          color: Color(0xff4a4a4a),
                          fontSize: 22,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        )),
                  ],
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.2,
              right: 0,
              child: Container(
                alignment: Alignment.center,
                height: MediaQuery.of(context).size.height * .82,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 80,
                    ),
                    Container(
                      width: 160,
                      height: 164,
                      padding: EdgeInsets.all(34),
                      decoration: new BoxDecoration(
                          color: Color(0xfff6f6f6),
                          borderRadius: BorderRadius.circular(32)),
                      child: Image.asset(
                        "assets/home-icons/transfer.png",
                        width: 92,
                        height: 92,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 30, bottom: 7),
                      child: Text("هذه الخدمة ستعمل قريبا",
                          style: TextStyle(
                            fontFamily: 'Cairo',
                            color: Color(0xff4a4a4a),
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.normal,
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 30),
                      child: Text("مازالت خدمة إدفع فواتيرك تحت التطوير",
                          style: TextStyle(
                            fontFamily: 'Cairo',
                            color: Color(0xff9b9b9b),
                            fontSize: 15,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.normal,
                          )),
                    ),
                    Opacity(
                      opacity: 0.17000000178813934,
                      child: Container(
                        width: 182,
                        height: 112,
                        child: Image.asset("assets/icons/kashNow.png"),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
