import 'package:flutter/material.dart';
import 'package:kash_now/features/sginUp/signup_view3.dart';

class InstallmentsMenu extends StatefulWidget {
  InstallmentsMenu({Key? key}) : super(key: key);

  @override
  _InstallmentsMenuState createState() => _InstallmentsMenuState();
}

class _InstallmentsMenuState extends State<InstallmentsMenu> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 25.0, horizontal: 25),
                child: Text("قائمة الأقساط",
                    style: TextStyle(
                      fontFamily: 'Cairo',
                      color: Color(0xff4a4a4a),
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    )),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 35, right: 25, left: 25),
                child: new Text("قائمة عرض الأقساط الخاصة بك",
                    style: TextStyle(
                      fontFamily: 'Cairo',
                      color: Color(0xff9b9b9b),
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                    )),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Row(
                  children: [
                    Container(
                      width: 30,
                      height: 30,
                      child: Image.asset("assets/home-icons/install.png"),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      child: Text("إجمالي الأقساط",
                          style: TextStyle(
                            fontFamily: 'Cairo',
                            color: Color(0xff595959),
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 80),
                      child: Stack(
                        children: <Widget>[
                          Opacity(
                            opacity: 0.07999999821186066,
                            child: new Container(
                                width: 108,
                                height: 44,
                                decoration: new BoxDecoration(
                                    color: Color(0xfff79131),
                                    borderRadius: BorderRadius.circular(12))),
                          ),
                          Positioned.fill(
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 25, vertical: 10),
                              child: new Text("3,300",
                                  style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    color: Color(0xfff79131),
                                    fontSize: 21,
                                    fontWeight: FontWeight.w600,
                                    fontStyle: FontStyle.normal,
                                  )),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 45,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 25),
                  width: 360,
                  height: 1,
                  decoration: BoxDecoration(color: Color(0xfff6f6f6))),
              SizedBox(
                height: 30,
              ),
              Container(
                margin: EdgeInsets.only(left: 20, right: 20),
                // padding: EdgeInsets.all(),
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      CurrentInstallmentCard(),
                      InstallmentCard(),
                      InstallmentCard(),
                      InstallmentCard(),
                      InstallmentCard(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CurrentInstallmentCard extends StatelessWidget {
  CurrentInstallmentCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 190,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 22),
            padding: EdgeInsets.symmetric(horizontal: 23, vertical: 6),
            width: 130,
            height: 36,
            decoration: new BoxDecoration(
              color: Color(0xff5eb300),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(14), topRight: Radius.circular(14)),
            ),
            child: Text("القسط الحالى",
                style: TextStyle(
                  fontFamily: 'Cairo',
                  color: Colors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal,
                )),
          ),
          Container(
            width: 360,
            height: 120,
            padding: EdgeInsets.symmetric(horizontal: 29, vertical: 14),
            decoration: new BoxDecoration(
                color: Color(0xfff9f9f9),
                borderRadius: BorderRadius.circular(18)),
            child: Row(
              children: [
                Column(
                  children: [
                    Stack(
                      children: <Widget>[
                        Opacity(
                          opacity: 0.07999999821186066,
                          child: new Container(
                              width: 35,
                              height: 36,
                              decoration: new BoxDecoration(
                                  color: Color(0xff5eb300),
                                  borderRadius: BorderRadius.circular(6))),
                        ),
                        Positioned.fill(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 7),
                            child: Image.asset("assets/home-icons/month.png"),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Stack(
                      children: <Widget>[
                        Opacity(
                          opacity: 0.07999999821186066,
                          child: new Container(
                              width: 35,
                              height: 36,
                              decoration: new BoxDecoration(
                                  color: Color(0xfff79131),
                                  borderRadius: BorderRadius.circular(6))),
                        ),
                        Positioned.fill(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 7),
                            child: Image.asset("assets/home-icons/bag.png"),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
                SizedBox(
                  width: 20,
                ),
                Column(
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 6),
                          child: Text(" يناير",
                              style: TextStyle(
                                fontFamily: 'Cairo',
                                color: Color(0xff4a4a4a),
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                        Text("2022",
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              color: Color(0xff4a4a4a),
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                            ))
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 6),
                          child: Text("350",
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                color: Color(0xff4a4a4a),
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                        new Text("جنيها",
                            style: TextStyle(
                              fontFamily: 'Cairo',
                              color: Color(0xff4a4a4a),
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            )),
                      ],
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class InstallmentCard extends StatelessWidget {
  const InstallmentCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 360,
      height: 190,
      margin: EdgeInsets.only(bottom: 15),
      decoration: new BoxDecoration(
        color: Color(0xfff9f9f9),
        borderRadius: BorderRadius.circular(18),
      ),
      child: Column(
        children: [
          SizedBox(
            height: 18,
          ),
          Container(
            width: 378,
            height: 100,
            padding: EdgeInsets.symmetric(
              horizontal: 29,
            ),
            decoration: new BoxDecoration(
                color: Color(0xfff9f9f9),
                borderRadius: BorderRadius.circular(18)),
            child: Row(
              children: [
                Column(
                  children: [
                    Stack(
                      children: <Widget>[
                        Opacity(
                          opacity: 0.07999999821186066,
                          child: new Container(
                              width: 35,
                              height: 36,
                              decoration: new BoxDecoration(
                                  color: Color(0xff5eb300),
                                  borderRadius: BorderRadius.circular(6))),
                        ),
                        Positioned.fill(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 7),
                            child: Image.asset("assets/home-icons/month.png"),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Stack(
                      children: <Widget>[
                        Opacity(
                          opacity: 0.07999999821186066,
                          child: new Container(
                              width: 35,
                              height: 36,
                              decoration: new BoxDecoration(
                                  color: Color(0xfff79131),
                                  borderRadius: BorderRadius.circular(6))),
                        ),
                        Positioned.fill(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 7),
                            child: Image.asset("assets/home-icons/bag.png"),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
                SizedBox(
                  width: 20,
                ),
                Column(
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 6),
                          child: Text(" يناير",
                              style: TextStyle(
                                fontFamily: 'Cairo',
                                color: Color(0xff4a4a4a),
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                        Text("2022",
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              color: Color(0xff4a4a4a),
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                            ))
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 6),
                          child: Text("350",
                              style: TextStyle(
                                fontFamily: 'Montserrat',
                                color: Color(0xff4a4a4a),
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                        new Text("جنيها",
                            style: TextStyle(
                              fontFamily: 'Cairo',
                              color: Color(0xff4a4a4a),
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            )),
                      ],
                    ),
                  ],
                )
              ],
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Container(
            width: 320,
            margin: EdgeInsets.symmetric(
              horizontal: 10,
            ),
            height: 52,
            padding: EdgeInsets.symmetric(vertical: 10),
            decoration: new BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(10)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                new Text("عرض التفاصيل",
                    style: TextStyle(
                      fontFamily: 'Cairo',
                      color: Color(0xfff79131),
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                    )),
                SizedBox(
                  width: 12,
                ),
                Container(
                    padding: EdgeInsets.only(top: 3),
                    width: 8.966796875,
                    height: 18.0797119140625,
                    child: Image.asset("assets/home-icons/left_arrow.png")),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
