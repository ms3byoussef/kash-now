import 'package:flutter/material.dart';
import 'package:kash_now/features/sginUp/signup_view3.dart';
import 'package:kash_now/widegts/text_field_custom.dart';

class SignUpView5 extends StatefulWidget {
  const SignUpView5({Key? key}) : super(key: key);

  @override
  _SignUpView5State createState() => _SignUpView5State();
}

class _SignUpView5State extends State<SignUpView5> {
  int selectedIndex = 0;
  TextEditingController fristName = TextEditingController();
  TextEditingController secName = TextEditingController();
  TextEditingController thdName = TextEditingController();
  TextEditingController furName = TextEditingController();
  TextEditingController nationalId = TextEditingController();
  TextEditingController salray = TextEditingController();
  TextEditingController employeNO = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 10,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.1,
                width: 24,
                // padding: EdgeInsets.all(),
                child: InkWell(
                  onTap: () {},
                  child: Image.asset("assets/icons/noun_back_2102859.png"),
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.15,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * .13,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "إنشاء حساب جديد",
                      style: TextStyle(
                        fontFamily: 'Cairo',
                        color: Color(0xff4a4a4a),
                        fontSize: 22,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "خامسا : قم بتصوير البطاقة الشخصية الخاصة بك",
                      style: TextStyle(
                        fontFamily: 'Cairo',
                        color: Color(0xff858585),
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.30,
              right: 0,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15),
                alignment: Alignment.topRight,
                height: MediaQuery.of(context).size.height * .6,
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.all(10),
                        width: 378,
                        height: 231,
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Color(0xff4a4a4a).withOpacity(.4),
                              width: 1),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                                margin: EdgeInsets.all(15),
                                width: 41.60136413574219,
                                height: 41.601356506347656,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "assets/icons/noun_Camera_4413591.png")),
                                )),
                            Text("صورة البطاقة الشخصية الأمامية",
                                style: TextStyle(
                                  fontFamily: 'Cairo',
                                  color: Color(0xff4a4a4a),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  fontStyle: FontStyle.normal,
                                )),
                            Text(
                                "قم بتصوير صورة أمامية للبطاقة الشخصية\n مع مراعاة وضوح جميع البيانات",
                                style: TextStyle(
                                  fontFamily: 'Cairo',
                                  color: Color(0xff7e7e7e),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                  fontStyle: FontStyle.normal,
                                ))
                          ],
                        ),
                      ),
                      Container(
                        width: 378,
                        height: 231,
                        margin: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: Color(0xff4a4a4a).withOpacity(.4),
                              width: 1),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                                margin: EdgeInsets.all(15),
                                width: 41.60136413574219,
                                height: 41.601356506347656,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "assets/icons/noun_Camera_4413591.png")),
                                )),
                            Text("صورة البطاقة الشخصية الخلفية",
                                style: TextStyle(
                                  fontFamily: 'Cairo',
                                  color: Color(0xff4a4a4a),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  fontStyle: FontStyle.normal,
                                )),
                            Text(
                                "قم بتصوير صورة خلفية للبطاقة الشخصية\n مع مراعاة وضوح جميع البيانات",
                                style: TextStyle(
                                  fontFamily: 'Cairo',
                                  color: Color(0xff7e7e7e),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                  fontStyle: FontStyle.normal,
                                ))
                          ],
                        ),
                      ),
                      NextBtn(
                        txetColor: Colors.white,
                        text: "التالى",
                        onTap: () {},
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
