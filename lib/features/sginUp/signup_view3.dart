import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kash_now/widegts/text_field_custom.dart';

class SignUpView3 extends StatefulWidget {
  const SignUpView3({Key? key}) : super(key: key);

  @override
  _SignUpView3State createState() => _SignUpView3State();
}

class _SignUpView3State extends State<SignUpView3> {
  int selectedIndex = 0;
  TextEditingController fristName = TextEditingController();
  TextEditingController secName = TextEditingController();
  TextEditingController thdName = TextEditingController();
  TextEditingController furName = TextEditingController();
  TextEditingController nationalId = TextEditingController();
  TextEditingController salray = TextEditingController();
  TextEditingController employeNO = TextEditingController();
  TextEditingController dateTime = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 10,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.1,
                width: 24,
                // padding: EdgeInsets.all(),
                child: InkWell(
                  child: Image.asset("assets/icons/noun_back_2102859.png"),
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.15,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * .13,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "إنشاء حساب جديد",
                      style: TextStyle(
                        fontFamily: 'Cairo',
                        color: Color(0xff4a4a4a),
                        fontSize: 22,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "ثالثا : قم بملأ بياناتك",
                      style: TextStyle(
                        fontFamily: 'Cairo',
                        color: Color(0xff858585),
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.30,
              right: 0,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 25),
                alignment: Alignment.topRight,
                height: MediaQuery.of(context).size.height * .6,
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 152,
                        height: 52,
                        margin: EdgeInsets.only(bottom: 10),
                        padding: EdgeInsets.all(12),
                        decoration: new BoxDecoration(
                            color: Color(0xfff9f9f9),
                            borderRadius: BorderRadius.circular(14)),
                        child: Text(
                          "بياناتك الشخصية",
                          style: TextStyle(
                            fontFamily: 'Cairo',
                            color: Color(0xff7e7e7e),
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: CustomTextField(
                              controller: fristName,
                              labelText: "الاسم الأول",
                              keyboardType: TextInputType.name,
                              textInputAction: TextInputAction.next,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: CustomTextField(
                              controller: secName,
                              labelText: "الاسم الثانى",
                              keyboardType: TextInputType.name,
                              textInputAction: TextInputAction.next,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: CustomTextField(
                              controller: thdName,
                              labelText: "الاسم الثالث",
                              keyboardType: TextInputType.name,
                              textInputAction: TextInputAction.next,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: CustomTextField(
                              controller: furName,
                              labelText: "الاسم الرابع",
                              keyboardType: TextInputType.name,
                              textInputAction: TextInputAction.next,
                            ),
                          ),
                        ],
                      ),
                      CustomTextField(
                        controller: nationalId,
                        keyboardType: TextInputType.number,
                        textInputAction: TextInputAction.next,
                        labelText: "الرقم القومى",
                      ),
                      DropDownField(
                        list: [],
                        hint: "الحالة الإجتماعية ",
                        icon: "assets/icons/noun_south_1808537.png",
                      ),
                      Container(
                        width: 152,
                        height: 52,
                        margin: EdgeInsets.only(bottom: 10),
                        padding: EdgeInsets.all(12),
                        decoration: new BoxDecoration(
                            color: Color(0xfff9f9f9),
                            borderRadius: BorderRadius.circular(14)),
                        child: Text("معلومات العمل",
                            style: TextStyle(
                              fontFamily: 'Cairo',
                              color: Color(0xff7e7e7e),
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            )),
                      ),
                      DropDownField(
                        list: [],
                        hint: "المهنة",
                        icon: "assets/icons/noun_south_1808537.png",
                      ),
                      Stack(
                        children: [
                          Positioned(
                            child: CustomTextField(
                              labelText: "موعد التوظيف ",
                              controller: dateTime,
                            ),
                          ),
                          Positioned.fill(
                            right: 200,
                            bottom: 20,
                            child: Row(
                              children: [
                                Stack(
                                  children: <Widget>[
                                    Opacity(
                                      opacity: 0.07999999821186066,
                                      child: new Container(
                                          width: 26,
                                          height: 26,
                                          decoration: new BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(50),
                                              color: Color(0xffe70744))),
                                    ),
                                    Positioned.fill(
                                      child: Container(
                                        padding: EdgeInsets.all(2),
                                        child: Image.asset(
                                            "assets/icons/Path 55512.png"),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text("اختيارى",
                                    style: TextStyle(
                                      fontFamily: 'Cairo',
                                      color: Color(0xffe70744),
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )),
                                SizedBox(
                                  width: 20,
                                ),
                                InkWell(
                                  onTap: () async {
                                    await pickDate(context);
                                  },
                                  child: Container(
                                    width: 24.26416015625,
                                    height: 20.364501953125,
                                    child: Image.asset(
                                      "assets/icons/noun_calender_3648199.png",
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      CustomTextField(
                        controller: salray,
                        keyboardType: TextInputType.number,
                        textInputAction: TextInputAction.next,
                        labelText: "الراتب",
                      ),
                      CustomTextField(
                        controller: employeNO,
                        labelText: "الرقم الوظيفى",
                        keyboardType: TextInputType.number,
                        textInputAction: TextInputAction.next,
                      ),
                      NextBtn(
                        txetColor: Colors.white,
                        text: "التالى",
                        onTap: () {},
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future pickDate(BuildContext context) async {
    final initialDate = DateTime.now();
    final newDate = await showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: DateTime(DateTime.now().day),
      lastDate: DateTime(DateTime.now().year + 1),
    );
    if (newDate == null) return;
    setState(() {
      dateTime.text = DateFormat.yMMMd().format(newDate).toString();
    });
  }
}

class NextBtn extends StatelessWidget {
  String? text;
  VoidCallback? onTap;
  Color? txetColor;

  NextBtn({
    this.text,
    this.onTap,
    this.txetColor,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: new Container(
        width: 378,
        height: 65,
        decoration: new BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          gradient: LinearGradient(
            colors: [Color(0xfff79131), Color(0xfff9a01b)],
            stops: [0, 1],
            begin: Alignment(-0.99, -0.10),
            end: Alignment(0.99, 0.10),
            // angle: 96,
            // scale: undefined,
          ),
          boxShadow: [
            BoxShadow(
                color: Color(0x247d7d7d),
                offset: Offset(0, 0),
                blurRadius: 11,
                spreadRadius: 0)
          ],
        ),
        child: Text(
          text!,
          style: TextStyle(
              color: txetColor ?? Colors.white,
              fontSize: 17,
              fontWeight: FontWeight.w700,
              fontFamily: "Cairo"),
        ),
        alignment: Alignment.center,
      ),
    );
  }
}

class DropDownField extends StatefulWidget {
  String? hint;
  String? icon;
  Color? txetColor;
  VoidCallback? onTap;

  List<DropdownMenuItem<dynamic>>? list;

  DropDownField(
      {this.icon, this.hint, this.txetColor, this.onTap, this.list, Key? key})
      : super(key: key);

  @override
  _DropDownFieldState createState() => _DropDownFieldState();
}

class _DropDownFieldState extends State<DropDownField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 378,
      height: 65,
      margin: EdgeInsets.only(left: 10, bottom: 10),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xff9b9b9b), width: 1),
        borderRadius: BorderRadius.circular(15),
      ),
      child: DropdownButton(
        onTap: widget.onTap,
        isExpanded: true,
        items: widget.list ?? [],
        icon: Image.asset(
          widget.icon!,
          fit: BoxFit.contain,
          width: 20,
          height: 22,
        ),
        hint: Text(widget.hint!,
            style: TextStyle(
              fontFamily: 'Cairo',
              color: widget.txetColor,
              fontSize: 18,
              fontWeight: FontWeight.w600,
            )),
      ),
    );
  }
}
