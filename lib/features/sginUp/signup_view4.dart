import 'package:flutter/material.dart';
import 'package:kash_now/features/sginUp/signup_view3.dart';
import 'package:kash_now/widegts/text_field_custom.dart';

class SignUpView4 extends StatefulWidget {
  const SignUpView4({Key? key}) : super(key: key);

  @override
  _SignUpView4State createState() => _SignUpView4State();
}

class _SignUpView4State extends State<SignUpView4> {
  int selectedIndex = 0;
  TextEditingController emilController = TextEditingController();
  TextEditingController secName = TextEditingController();
  TextEditingController thdName = TextEditingController();
  TextEditingController furName = TextEditingController();
  TextEditingController nationalId = TextEditingController();
  TextEditingController salray = TextEditingController();
  TextEditingController employeNO = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 10,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.1,
                width: 24,
                // padding: EdgeInsets.all(),
                child: InkWell(
                  child: Image.asset("assets/icons/noun_back_2102859.png"),
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.15,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * .13,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "إنشاء حساب جديد",
                      style: TextStyle(
                        fontFamily: 'Cairo',
                        color: Color(0xff4a4a4a),
                        fontSize: 22,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "رابعا : قم بملأ بيانات التواصل",
                      style: TextStyle(
                        fontFamily: 'Cairo',
                        color: Color(0xff858585),
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.30,
              right: 0,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15),
                alignment: Alignment.topRight,
                height: MediaQuery.of(context).size.height * .6,
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // CustomTextField(
                      //   controller: ,
                      //   labelText: "البريد الإلكترونى",
                      //   keyboardType: TextInputType.emailAddress,
                      //   textInputAction: TextInputAction.next,
                      // ),
                      Stack(
                        children: [
                          Positioned(
                            child: CustomTextField(
                              controller: emilController,
                              labelText: "البريد الإلكترونى",
                              keyboardType: TextInputType.emailAddress,
                              textInputAction: TextInputAction.next,
                            ),
                          ),
                          Positioned.fill(
                            right: 250,
                            bottom: 20,
                            child: Row(
                              children: [
                                Stack(
                                  children: <Widget>[
                                    Opacity(
                                      opacity: 0.07999999821186066,
                                      child: new Container(
                                          width: 26,
                                          height: 26,
                                          decoration: new BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(50),
                                              color: Color(0xffe70744))),
                                    ),
                                    Positioned.fill(
                                      child: Container(
                                        padding: EdgeInsets.all(2),
                                        child: Image.asset(
                                            "assets/icons/Path 55512.png"),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text("اختيارى",
                                    style: TextStyle(
                                      fontFamily: 'Cairo',
                                      color: Color(0xffe70744),
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )),
                                SizedBox(
                                  width: 20,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      CustomTextField(
                        controller: furName,
                        labelText: " رقم الهاتف",
                        keyboardType: TextInputType.phone,
                        textInputAction: TextInputAction.next,
                      ),
                      DropDownField(
                        list: [],
                        hint: " المدينة ",
                        icon: "assets/icons/noun_south_1808537.png",
                      ),
                      DropDownField(
                        list: [],
                        hint: " الحى / المنطقة ",
                        icon: "assets/icons/noun_south_1808537.png",
                      ),
                      CustomTextField(
                        controller: furName,
                        labelText: "اكتب عنوانك التفصيلى",
                        keyboardType: TextInputType.streetAddress,
                        textInputAction: TextInputAction.next,
                      ),
                      Text(
                        "الشخص المرجعى",
                        style: TextStyle(
                          fontFamily: 'Cairo',
                          color: Color(0xff4a4a4a),
                          fontSize: 18,
                          fontWeight: FontWeight.w800,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                      new Text("قم باختيار شخص من جهات الإتصال على هاتفك",
                          style: TextStyle(
                            fontFamily: 'Cairo',
                            color: Color(0xffbebebe),
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.normal,
                          )),
                      Stack(
                        children: <Widget>[
                          Opacity(
                            opacity: 0.09000000357627869,
                            child: new Container(
                                margin: EdgeInsets.all(10),
                                width: 163,
                                height: 52,
                                decoration: new BoxDecoration(
                                    color: Color(0xff5eb300),
                                    borderRadius: BorderRadius.circular(10))),
                          ),
                          Positioned.fill(
                            child: Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Image.asset(
                                    "assets/icons/noun_contact_2385597.png",
                                    width: 35,
                                    height: 35,
                                  ),
                                ),
                                Text("جهات الإتصال",
                                    style: TextStyle(
                                      fontFamily: 'Cairo',
                                      color: Color(0xff5eb300),
                                      fontSize: 18,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                    )),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.all(10),
                        width: 378,
                        height: 110,
                        decoration: new BoxDecoration(
                            color: Color(0xfff9f9f9),
                            borderRadius: BorderRadius.circular(15)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Container(
                                      width: 20,
                                      height: 20,
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 14),
                                      child: Image.asset(
                                          "assets/icons/noun_contact_3888001.png"),
                                    ),
                                    Text("الاسم",
                                        style: TextStyle(
                                          fontFamily: 'Cairo',
                                          color: Color(0xff7e7e7e),
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                          fontStyle: FontStyle.normal,
                                        )),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Container(
                                      width: 20,
                                      height: 20,
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 14),
                                      child: Image.asset(
                                          "assets/icons/Group 8628.png"),
                                    ),
                                    Text("رقم الهاتف",
                                        style: TextStyle(
                                          fontFamily: 'Cairo',
                                          color: Color(0xff7e7e7e),
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                          fontStyle: FontStyle.normal,
                                        )),
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Row(
                                    children: [
                                      Container(
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 8),
                                          width: 1,
                                          height: 22,
                                          color: Color(0xffc6c6c6)),
                                      Text(" خالد يوسف ",
                                          style: TextStyle(
                                            fontFamily: 'Cairo',
                                            color: Color(0xff4a4a4a),
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600,
                                            fontStyle: FontStyle.normal,
                                          )),
                                    ],
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Row(
                                    children: [
                                      Container(
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 8),
                                          width: 1,
                                          height: 22,
                                          color: Color(0xffc6c6c6)),
                                      Text("01098959774 ",
                                          style: TextStyle(
                                            fontFamily: 'Cairo',
                                            color: Color(0xff4a4a4a),
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600,
                                            fontStyle: FontStyle.normal,
                                          )),
                                    ],
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      NextBtn(
                        txetColor: Colors.white,
                        text: "التالى",
                        onTap: () {},
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
