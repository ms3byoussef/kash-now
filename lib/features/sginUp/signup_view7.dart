import 'package:flutter/material.dart';
import 'package:kash_now/features/sginUp/signup_view3.dart';
import 'package:kash_now/widegts/text_field_custom.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class SignUpView7 extends StatefulWidget {
  const SignUpView7({Key? key}) : super(key: key);

  @override
  _SignUpView7State createState() => _SignUpView7State();
}

class _SignUpView7State extends State<SignUpView7> {
  int selectedIndex = 0;
  TextEditingController password = TextEditingController();
  TextEditingController comfirmPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 10,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * .05,
                width: 24,
                // padding: EdgeInsets.all(),
                child: InkWell(
                  child: Image.asset("assets/icons/noun_back_2102859.png"),
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.12,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * .34,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    new Text("تأكيد رقم الهاتف",
                        style: TextStyle(
                          fontFamily: 'Cairo',
                          color: Color(0xff4a4a4a),
                          fontSize: 22,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        )),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      width: 147,
                      height: 165.4,
                      child: Image.asset(
                        "assets/icons/v.png",
                        width: 120,
                        height: 120,
                        fit: BoxFit.cover,
                      ),
                      // decoration: BoxDecoration(
                      //     image: DecorationImage(
                      //         image: AssetImage(
                      //           "assets/icons/v.png",
                      //         ),
                      //         fit: BoxFit.contain)),
                    )
                  ],
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.48,
              right: 0,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15),
                alignment: Alignment.topRight,
                height: MediaQuery.of(context).size.height * .48,
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                          "قم بكتابة كود التحقق التي تم إرساله إليك عبر\n رقم الهاتف",
                          style: TextStyle(
                            fontFamily: 'Cairo',
                            color: Color(0xff9b9b9b),
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.normal,
                          )),
                      SizedBox(
                        height: 30,
                      ),
                      Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              _showEditPhoneBottomSheet(context);
                            },
                            child: Stack(
                              children: <Widget>[
                                Opacity(
                                  opacity: 0.09000000357627869,
                                  child: new Container(
                                      width: 37,
                                      height: 37,
                                      decoration: new BoxDecoration(
                                          color: Color(0xfff79131),
                                          borderRadius:
                                              BorderRadius.circular(10))),
                                ),
                                Positioned.fill(
                                  child: Container(
                                    padding: EdgeInsets.all(8.9),
                                    width: 19.1632080078125,
                                    height: 19.16265869140625,
                                    child: Image.asset(
                                      "assets/icons/noun_edit_1372808.png",
                                      width: 18,
                                      height: 18,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Text("+20 1098959774",
                                style: TextStyle(
                                  fontFamily: 'Cairo',
                                  color: Color(0xff4a4a4a),
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600,
                                  fontStyle: FontStyle.normal,
                                )),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 17,
                      ),
                      PinCodeTextField(
                        appContext: context,
                        length: 5,
                        onChanged: (value) {
                          print(value);
                        },
                        pinTheme: PinTheme(
                            shape: PinCodeFieldShape.box,
                            borderRadius: BorderRadius.circular(25),
                            fieldOuterPadding: EdgeInsets.only(right: 1),
                            selectedColor: Color(0xff5eb300),
                            activeColor: Color(0xffe3e3e3),
                            inactiveFillColor: Color(0xff5eb300),
                            inactiveColor: Color(0xff5eb300),
                            selectedFillColor: Color(0xff5eb300),
                            activeFillColor: Color(0xff5eb300),
                            fieldWidth: 67,
                            fieldHeight: 71),
                      ),
                      SizedBox(height: 20),
                      NextBtn(
                        txetColor: Colors.white,
                        text: "تحقق",
                        onTap: () {},
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: Text("إنتظر لطلب إرسال كود مرة أخرى إذا لم يصلك",
                            style: TextStyle(
                              fontFamily: 'Cairo',
                              color: Color(0xff4a4a4a),
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            )),
                      ),
                      Stack(
                        children: <Widget>[
                          Opacity(
                            opacity: 0.09000000357627869,
                            child: new Container(
                                width: 378,
                                height: 65,
                                decoration: new BoxDecoration(
                                    color: Color(0xfff79131),
                                    borderRadius: BorderRadius.circular(15))),
                          ),
                          Positioned.fill(
                            child: MaterialButton(
                              onPressed: () {},
                              child: new Text(
                                "أرسل الكود مرة أخرى",
                                style: TextStyle(
                                  fontFamily: 'Cairo',
                                  color: Color(0xfff79131),
                                  fontSize: 17,
                                  fontWeight: FontWeight.w600,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 30),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _showEditPhoneBottomSheet(context) {
    return showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Directionality(
            textDirection: TextDirection.rtl,
            child: Container(
              width: 428,
              height: 364,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 45,
                    ),
                    Row(
                      children: [
                        Stack(
                          children: <Widget>[
                            Opacity(
                              opacity: 0.09000000357627869,
                              child: new Container(
                                  width: 39,
                                  height: 39,
                                  decoration: new BoxDecoration(
                                      color: Color(0xfff79131),
                                      borderRadius: BorderRadius.circular(10))),
                            ),
                            Positioned.fill(
                              child: Container(
                                padding: EdgeInsets.all(8.9),
                                width: 20,
                                height: 20,
                                child: Image.asset(
                                  "assets/icons/noun_edit_1372808.png",
                                  width: 18,
                                  height: 18,
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text("تعديل رقم الهاتف",
                            style: TextStyle(
                              fontFamily: 'Cairo',
                              color: Color(0xff4a4a4a),
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            )),
                      ],
                    ),
                    SizedBox(
                      height: 17,
                    ),
                    Text("قم بتعديل رقم الهاتف لإرسال كود التحقق",
                        style: TextStyle(
                          fontFamily: 'Cairo',
                          color: Color(0xff9b9b9b),
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                          fontStyle: FontStyle.normal,
                        )),
                    SizedBox(
                      height: 34,
                    ),
                    CustomTextField(
                      labelText: "رقم الهاتف",
                      keyboardType: TextInputType.phone,
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    NextBtn(
                      text: "حفظ التعديل",
                      onTap: () {},
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
}
