import 'package:flutter/material.dart';
import 'package:kash_now/features/sginUp/signup_view3.dart';
import 'package:kash_now/widegts/text_field_custom.dart';

class SignUpView1 extends StatefulWidget {
  const SignUpView1({Key? key}) : super(key: key);

  @override
  _SignUpView1State createState() => _SignUpView1State();
}

class _SignUpView1State extends State<SignUpView1> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 10,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.1,
                width: 24,
                // padding: EdgeInsets.all(),
                child: InkWell(
                  child: Image.asset("assets/icons/noun_back_2102859.png"),
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.15,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * .2,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "إنشاء حساب جديد",
                      style: TextStyle(
                        fontFamily: 'Cairo',
                        color: Color(0xff4a4a4a),
                        fontSize: 22,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "أولا : قم باختيار الشركة",
                      style: TextStyle(
                        fontFamily: 'Cairo',
                        color: Color(0xff858585),
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.3,
              right: 0,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 25),
                height: MediaQuery.of(context).size.height * 0.1,
                width: MediaQuery.of(context).size.width,
                // padding: EdgeInsets.all(),
                child: CustomTextField(
                  keyboardType: TextInputType.text,
                  labelText: "قم بالبحث عن الشركة",
                  preIcon: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Image.asset(
                      "assets/icons/noun_Search_875357.png",
                      width: 20,
                      height: 22,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.38,
              right: 0,
              child: Container(
                alignment: Alignment.topRight,
                height: MediaQuery.of(context).size.height * .62,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          alignment: Alignment.centerRight,
                          height: MediaQuery.of(context).size.height * .62,
                          width: 30,
                        ),
                        Container(
                            alignment: Alignment.centerRight,
                            height: MediaQuery.of(context).size.height * .6,
                            width: MediaQuery.of(context).size.width - 30,
                            child: GridViewCustom()),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).size.height * .06,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: NextBtn(
                  txetColor: Colors.white,
                  text: "التالى",
                  onTap: () {},
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class GridViewCustom extends StatefulWidget {
  const GridViewCustom({Key? key}) : super(key: key);

  @override
  _GridViewCustomState createState() => _GridViewCustomState();
}

class _GridViewCustomState extends State<GridViewCustom> {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: 10,
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            crossAxisSpacing: 30,
            mainAxisSpacing: 80,
            childAspectRatio: .6),
        itemBuilder: (context, index) {
          return ComCard(
            name: "asas",
            selected: false,
            img: "",
            onTap: () {},
          );
        });
  }
}

class ComCard extends StatefulWidget {
  String? img;
  String? name;
  VoidCallback? onTap;
  bool selected = false;

  ComCard({this.img, this.name, this.onTap, required this.selected, Key? key})
      : super(key: key);

  @override
  _ComCardState createState() => _ComCardState();
}

class _ComCardState extends State<ComCard> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          widget.selected = true;
        });
      },
      child: Container(
        width: 120,
        height: 120,
        // margin: EdgeInsets.all(10),
        decoration: widget.selected
            ? BoxDecoration(
                color: Color(0x1f5eb300),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(140),
                    topRight: Radius.circular(140),
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20)),
                boxShadow: [
                  BoxShadow(
                      color: Color(0x59d9d9d9),
                      offset: Offset(0, 0),
                      blurRadius: 10,
                      spreadRadius: 1)
                ],
              )
            : null,
        child: Column(
          children: <Widget>[
            Container(
              width: 125,
              height: 100,
              padding: EdgeInsets.all(10),
              decoration: new BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Color(0x59d9d9d9),
                    blurRadius: 20,
                  )
                ],
              ),
              child: Image.asset(
                "assets/icons/1024px-Zara_Logo.svg.png",
                fit: BoxFit.contain,
                width: 60,
                height: 60,
              ),
            ),
            Text(
              widget.name!,
              style: TextStyle(
                fontFamily: 'Cairo',
                color: widget.selected ? Color(0xff5eb300) : Color(0xff4a4a4a),
                fontSize: 16,
                fontWeight: FontWeight.w600,
                fontStyle: FontStyle.normal,
              ),
            ),
            widget.selected
                ? Container(
                    padding: EdgeInsets.all(6),
                    width: 33,
                    height: 33,
                    child: Image.asset(
                      "assets/icons/noun_tick_3968147.png",
                      width: 28,
                      height: 28,
                      fit: BoxFit.cover,
                    ),
                    decoration: BoxDecoration(
                        color: Color(0xff5eb300),
                        borderRadius: BorderRadius.circular(10)))
                : Container(),
          ],
        ),
      ),
    );
  }
}

// class AlphabetScrollView extends StatefulWidget {
//   const AlphabetScrollView({Key? key}) : super(key: key);

//   @override
//   _AlphabetScrollViewState createState() => _AlphabetScrollViewState();
// }

// class _AlphabetScrollViewState extends State<AlphabetScrollView> {
//   @override
//   Widget build(BuildContext context) {
//     return AzListView(
//         data: list,
//         itemCount: list.length,
//         itemBuilder: (context, index) {
//           final item = list[index];
//         });
//   }
// }
