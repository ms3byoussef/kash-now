import 'package:flutter/material.dart';
import 'package:kash_now/features/sginUp/signup_view3.dart';

class SignUpView2 extends StatefulWidget {
  const SignUpView2({Key? key}) : super(key: key);

  @override
  _SignUpView2State createState() => _SignUpView2State();
}

class _SignUpView2State extends State<SignUpView2> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 10,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.1,
                width: 24,
                // padding: EdgeInsets.all(),
                child: InkWell(
                  child: Image.asset("assets/icons/noun_back_2102859.png"),
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.1,
              right: 25,
              child: Container(
                height: MediaQuery.of(context).size.height * .34,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "إنشاء حساب جديد",
                      style: TextStyle(
                        fontFamily: 'Cairo',
                        color: Color(0xff4a4a4a),
                        fontSize: 22,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "ثانيا : قم باختيار فرع الشركة",
                      style: TextStyle(
                        fontFamily: 'Cairo',
                        color: Color(0xff858585),
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        new Container(
                          padding: EdgeInsets.all(20),
                          width: 100,
                          height: 100,
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            image: DecorationImage(
                                image: AssetImage(
                                  "assets/icons/1024px-Zara_Logo.svg.png",
                                ),
                                fit: BoxFit.contain),
                            borderRadius: BorderRadius.circular(168),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0x59d9d9d9),
                                  offset: Offset(0, 0),
                                  blurRadius: 20,
                                  spreadRadius: 0)
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Column(
                          children: [
                            new Text("شركة زارا",
                                style: TextStyle(
                                  fontFamily: 'Cairo',
                                  color: Color(0xff4a4a4a),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                )),
                            Text("الشركة التي تم اختيارها",
                                style: TextStyle(
                                  fontFamily: 'Cairo',
                                  color: Color(0xfff79131),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                  fontStyle: FontStyle.normal,
                                ))
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.4,
              right: 0,
              child: Container(
                alignment: Alignment.center,
                height: MediaQuery.of(context).size.height * .6,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      height: MediaQuery.of(context).size.height * .58,
                      width: MediaQuery.of(context).size.width - 50,
                      child: ListView(
                        children: [
                          ListCard(
                            isSelected: true,
                          ),
                          ListCard(),
                          ListCard(),
                          ListCard(),
                          ListCard(),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).size.height * .05,
              width: MediaQuery.of(context).size.width - 50,
              right: 25,
              child: Center(
                child: NextBtn(
                  txetColor: Colors.white,
                  text: "التالى",
                  onTap: () {},
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ListCard extends StatefulWidget {
  bool? isSelected = false;
  String? img;
  ListCard({this.isSelected, this.img, Key? key}) : super(key: key);

  @override
  _ListCardState createState() => _ListCardState();
}

class _ListCardState extends State<ListCard> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          widget.isSelected = true;
        });
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        width: 378,
        height: 108,
        decoration: widget.isSelected ?? false == true
            ? BoxDecoration(
                color: Color(0xffeaf5df),
                borderRadius: BorderRadius.circular(22),
              )
            : BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(22),
                boxShadow: [
                  BoxShadow(
                      color: Color(0x59d9d9d9),
                      offset: Offset(0, 0),
                      blurRadius: 20,
                      spreadRadius: 0)
                ],
              ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              margin: EdgeInsets.all(10),
              width: 78,
              height: 76,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(18),
                boxShadow: [
                  BoxShadow(
                      color: Color(0x59d9d9d9),
                      offset: Offset(0, 0),
                      blurRadius: 20,
                      spreadRadius: 0)
                ],
              ),
              child: Image.asset(
                  widget.img ?? "assets/icons/1024px-Zara_Logo.svg.png"),
            ),
            Text("فرع مول القاهرة",
                style: TextStyle(
                  fontFamily: 'Cairo',
                  color: widget.isSelected ?? false == true
                      ? Color(0xff5eb300)
                      : Color(0xff4a4a4a),
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.normal,
                )),
            SizedBox(
              width: 20,
            ),
            widget.isSelected ?? false == true
                ? new Container(
                    margin: EdgeInsets.only(left: 30),
                    padding: EdgeInsets.all(5),
                    width: 37,
                    height: 39,
                    child: Image.asset(
                      "assets/icons/noun_tick_3968147.png",
                      width: 30,
                      height: 30,
                      fit: BoxFit.contain,
                    ),
                    decoration: new BoxDecoration(
                        color: Color(0xff5eb300),
                        borderRadius: BorderRadius.circular(10)))
                : Container()
          ],
        ),
      ),
    );
  }
}
