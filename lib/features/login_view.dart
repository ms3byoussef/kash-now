import 'package:animated_toggle_switch/animated_toggle_switch.dart';
import 'package:flutter/material.dart';
import 'package:kash_now/widegts/text_field_custom.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  TextEditingController nationalId = TextEditingController();
  TextEditingController pinCode = TextEditingController();
  bool positive = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffffffff),
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.3,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Image.asset(
                      "assets/icons/Mask Group 1.png",
                      fit: BoxFit.cover,
                      height: 194,
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 40),
                      child: Image.asset(
                        "assets/icons/kashNow.png",
                        width: 182,
                        height: 112,
                      ),
                    ),
                    Image.asset(
                      "assets/icons/Mask Group 2.png",
                      fit: BoxFit.fitHeight,
                      height: 194,
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.27,
              bottom: 0,
              right: 0,
              left: 0,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.2,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 22,
                    ),
                    Text(
                      "تسجيل الدخول",
                      style: TextStyle(
                          color: Color(0xff4a4a4a),
                          fontSize: 22,
                          fontFamily: "Cairo",
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 7,
                    ),
                    Text(
                      " قم بتسجيل الدخول إلى حسابك",
                      style: TextStyle(
                          color: Color(0xff9b9b9b),
                          fontSize: 16,
                          fontWeight: FontWeight.w400),
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    LangWidget()
                  ],
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.52,
              bottom: 0,
              right: 0,
              left: 0,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                height: MediaQuery.of(context).size.height * 0.5,
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      CustomTextField(
                        labelText: "أدخل الرقم القومى",
                        controller: nationalId,
                        keyboardType: TextInputType.number,
                      ),
                      CustomTextField(
                        labelText: "أدخل الرقم السرى",
                        controller: pinCode,
                        keyboardType: TextInputType.number,
                        obscure: true,
                        suffix: false,
                        textInputAction: TextInputAction.done,
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 31),
                        alignment: Alignment.centerLeft,
                        child: GestureDetector(
                            onTap: () {},
                            child: Text(
                              "هل نسيت الرقم السرى ؟",
                              style: TextStyle(
                                  color: Color(0xff707070),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400),
                            )),
                      ),
                      MaterialButton(
                        onPressed: () {},
                        child: Container(
                          width: 378,
                          height: 65,
                          margin: EdgeInsets.only(top: 10, bottom: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            gradient: LinearGradient(
                              colors: [Color(0xfff79131), Color(0xfff9a01b)],
                              stops: [0, 1],
                              begin: Alignment(-0.99, -0.10),
                              end: Alignment(0.99, 0.10),
                              // angle: 96,
                              // scale: undefined,
                            ),
                          ),
                          child: Text(
                            "الدخول",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 22,
                                fontWeight: FontWeight.w700,
                                fontFamily: "Cairo"),
                          ),
                          alignment: Alignment.center,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      MaterialButton(
                        onPressed: () {},
                        child: Stack(
                          children: <Widget>[
                            Opacity(
                              opacity: 0.09000000357627869,
                              child: new Container(
                                  width: 378,
                                  height: 65,
                                  decoration: new BoxDecoration(
                                      color: Color(0xfff79131),
                                      borderRadius: BorderRadius.circular(15))),
                            ),
                            Positioned.fill(
                              child: Container(
                                margin: EdgeInsets.only(top: 10, bottom: 10),
                                child: Text(
                                  "إنشاء حساب جديد",
                                  style: TextStyle(
                                    fontFamily: 'Cairo',
                                    color: Color(0xfff79131),
                                    fontSize: 17,
                                    fontWeight: FontWeight.w600,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                                alignment: Alignment.center,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 60,
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class LangWidget extends StatelessWidget {
  const LangWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Opacity(
          opacity: 0.09000000357627869,
          child: new Container(
              width: 161,
              height: 45,
              decoration: new BoxDecoration(
                  color: Color(0xff858585),
                  borderRadius: BorderRadius.circular(15))),
        ),
        Positioned.fill(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              new Text("العربية",
                  style: TextStyle(
                    fontFamily: 'Cairo',
                    color: Color(0xff858585),
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.normal,
                  )),
              SizedBox(
                width: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Image.asset(
                  "assets/icons/Group 1.png",
                  width: 20.3,
                  height: 25,
                ),
              )
            ],
          ),
        ),
        Positioned(
          top: -2,
          bottom: -2,
          right: -3.5,
          child: Container(
            width: 50,
            padding: EdgeInsets.all(4.2),
            height: 50,
            child: Image.asset(
              "assets/icons/saudi-arabia.png",
              width: 33,
              height: 33,
              fit: BoxFit.contain,
            ),
            decoration: new BoxDecoration(
                color: Color(0xfff4f4f4),
                border: Border.all(color: Colors.white, width: 4),
                shape: BoxShape.circle),
          ),
        ),
      ],
    );
  }
}
